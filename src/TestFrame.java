import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TestFrame extends JFrame {

	private JPanel contentPane;
	private JTextField txtHelloWorld;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestFrame frame = new TestFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestFrame() {
		setTitle("My Test Frame");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JButton btnNewButton = new JButton("North");
		
		//defined and register an event handler by using
		//an anonymous inner class
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtHelloWorld.setText("You clicked NORTH");
			}
		});
		contentPane.add(btnNewButton, BorderLayout.NORTH);
		
		JButton btnSouth = new JButton("South");
		//registered event handler using named inner class
		btnSouth.addActionListener( new ClickListener("You clicked SOUTH") );
		contentPane.add(btnSouth, BorderLayout.SOUTH);
		
		JButton btnEast = new JButton("East");
		//registered event handler using named inner class

		btnEast.addActionListener(new AnotherClickListener());
		contentPane.add(btnEast, BorderLayout.EAST);
		
		JButton btnWest = new JButton("West");
		// Lambda expression are anonymous function that can be 
		//passed as arguments to methods -- they allow you to
		// treat function as data
		
		//You can pass a lambda expression to a method that accepts 
		// a FUNCTIIONAL INTERFACE as a parameter.(A functional interface
		//works like a normal interface, except it can contain only
		// one method.)
		
		//We converted the anonymous inner class definition to
		// a lambda expression using CTRL+1 shortcut (Command + 1)
		btnWest.addActionListener(e -> txtHelloWorld.setText(" You clicked WEST"));
		contentPane.add(btnWest, BorderLayout.WEST);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);// if null uses ABSOLUTE LAYOUT
		
		txtHelloWorld = new JTextField();
		txtHelloWorld.setText("Hello World");
		txtHelloWorld.setBounds(0, 11, 304, 20);
		panel.add(txtHelloWorld);
		txtHelloWorld.setColumns(10);
	}// end constructor for TestFrame
	
	class ClickListener implements ActionListener
	{
		String message;
		
		public ClickListener( String message)
		{
			this.message = message;
		}//end constructor for Click listener
		
		public void actionPerformed(ActionEvent arg0) {
			txtHelloWorld.setText(message);
		}
	}//end inner class ClickerListener
	
	class AnotherClickListener implements ActionListener
	{
		
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			JOptionPane.showMessageDialog(contentPane, "You clicked a button! ");
			
		}
	}//end inner class AnotherClickListener
	
}//end class TestFrame
